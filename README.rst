.. highlight:: shell

============
Djam Library
============

Djam is a generic library containing some frequently-used blocks for Django
apps.


Local development
-----------------

The recommended way is to have a virtualenv. Note that the project ``Makefile``
is be default set to use the virtualenv. You can disable this behaviour by
setting the ``ENV`` variable in the ``Makefile`` to something like
``ENV := NOENV=true``. Note that project conventions suggest that you keep
your venv inside the VCS root.

Clone the repo:

.. code-block:: console

    $ git clone git@bitbucket.org:evenlights/evenlights-djam.git


Create the environment:

.. code-block:: console
    $ cd evenlights-djam
    $ virtualenv -p python3 venv


Install development packages:

.. code-block:: console

    $ source ./venv/bin/activate
    $ pip install -r requirements_dev.txt

The ``release`` make target requires that you have the aws-cli_ package
installed and configured.


Release flow
------------

* create a release branch, desirably using a gitflow plugin

* bump the version:

.. code-block::: console

  $ bumpversion {major/minor/patch}

* update ``HISTORY.rst`` describing changes that this release introduces

* commit, publish the branch

* create a pull request

* run the ``release`` make target

* check things once again, at this point you still can upload the release
  without version bumping

* finish the release using gitflow, push branches


Features
--------

The library includes generic components for the following packages:

* conf: env setting utils

* backends: storage and login backends

* core: fields, filters, errors, generators, pagination, meta-information
  handling, routers, views and other useful stuff

* permissions: some common permission logic blocks

* service: common service views


Credits
-------

This package was created with Cookiecutter_ and the
`audreyr/cookiecutter-pypackage`_ project template.

.. _aws-cli: https://docs.aws.amazon.com/cli/latest/userguide/installing.html
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
