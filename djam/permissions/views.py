from django.db.models import Q
from django.http import Http404
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission

from rest_framework import views, generics
from rest_framework.response import Response

User = get_user_model()


class PermissionView(views.APIView):
    queryset = Permission.objects.all().select_related('content_type')

    def get_user(self):
        if 'user_pk' in self.kwargs:
            return generics.get_object_or_404(
                User, pk=self.kwargs.get('user_pk', None)
            )
        elif not self.request.user.is_anonymous:
            return self.request.user
        else:
            raise Http404

    def get(self, request, **kwargs):
        user = self.get_user()
        perms = [
            '{0!s}'.format(p.codename)
            for p in self.queryset.filter(Q(user=user) | Q(group__user=user))
        ]
        return Response(perms)
