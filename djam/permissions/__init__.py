"""
Concepts
========

Loose permissions
-----------------
Loose permissions allow viewing and editing objects without having those
explicitly assigned. The philosophy behind this pattern is that creation and
deletion of objects is only allowed for a certain group of people. E.g.
administrators can create or delete users but regular users cannot::

    {
        'GET': [],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': [],
        'PATCH': [],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

On the other hand, they can view and update objects. This means that e.g. users
may edit their profiles without an explicit permission. The list of the objects
they are allowed to see can be limited through filters, e.g. ownership filters
so that the users can only see themselves.

Thus, the term "loose permissions" describes a pattern where users may by
default see and edit objects, but not create or delete. The
:func:`get_required_permissions` filter backend method (model permissions) uses
the loose pattern.
As compared to the default DRF :class:`DjangoModelPermissions` and
:class:`DjangoModelPermissions`, loose permissions is a more permissive
setting::

    # DjangoModelPermissions and DjangoObjectPermissions
    {
        'GET': [],
        'OPTIONS': [],
        'HEAD': [],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

Strict permissions
------------------
Strict permissions mean that the user is only allowed to perform actions they
are explicitly assigned::

    {
        'GET': ['%(app_label)s.view_%(model_name)s'],
        'OPTIONS': ['%(app_label)s.view_%(model_name)s'],
        'HEAD': ['%(app_label)s.view_%(model_name)s'],
        'POST': ['%(app_label)s.add_%(model_name)s'],
        'PUT': ['%(app_label)s.change_%(model_name)s'],
        'PATCH': ['%(app_label)s.change_%(model_name)s'],
        'DELETE': ['%(app_label)s.delete_%(model_name)s'],
    }

Strict permissions is meant to be used on the views where there are no
exceptions to the rules. The :func:`get_required_object_permissions` filter
backend method (object permissions) uses the strict pattern.

Usage
=====

Ownership-based editing and viewing
-----------------------------------
An ownership-based editing and viewing is a pattern where users may only see and
edit objects that they own.

In order to enable a view to carry out the required permissions we'll generally
need one thing, or two depending on the requirements: a filter and a
custom mutation serializer.

To restrict the scope of objects that the user is allowed to see there is a
:class:`OwnerOrModelPermissionsFilter` class which should be added to the
permission classes of the view.

Optionally, you might want to override the :func:`get_serializer_class` method
of the view to use a custom serializer for mutation actions::

    def get_serializer_class(self):

        # ownership based mutation
        if self.request.method in ('PUT', 'PATCH') \
                and not self.request.user.has_perm('change_user'):
            return UserOwnershipBasedEditSerializer

        return super().get_serializer_class()

"""
