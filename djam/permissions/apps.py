from django.apps import AppConfig


class PermissionsConfig(AppConfig):
    name = 'djam.permissions'
    label = 'djam_permissions'
