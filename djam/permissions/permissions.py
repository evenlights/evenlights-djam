from rest_framework.permissions import BasePermission

from djam.permissions.utils import is_owner


class IsNotSuspended(BasePermission):
    def has_permission(self, request, view):
        if request.user and \
                hasattr(request.user, 'meta') \
                and request.user.meta is not None \
                and request.user.meta.get('suspended') is not None:
            return request.user.meta.get('suspended')

        return True


class IsOwner(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user:
            return is_owner(request.user, obj)
        return False
