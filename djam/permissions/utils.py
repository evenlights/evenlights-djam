from django.db.models import Q
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission

User = get_user_model()


def ownership_based_filter(user, queryset):
    """
    Queryset filtering function based on the ownership.
    :param user: user to filter owned objects for
    :param queryset: queryset to filter
    :return:
    """
    model_cls = queryset.model

    # objects belonging to the user
    if hasattr(model_cls, 'user'):
        return queryset.filter(user=user)

    # users can see themselves without the explicit permissions
    elif model_cls is User:
        return queryset.filter(pk=user.pk)

    else:
        return queryset.none()


def is_owner(user, obj):
    """
    Function to determine whether the object belongs to the user or not.
    :param user: user to test the ownership against
    :param obj: object to test the ownership for
    :return:
    """
    # the object belongs to the user
    if hasattr(obj, 'user'):
        return obj.user == user

    # users have permission for themselves
    elif isinstance(obj, User):
        return obj == user

    return False


def get_user_permission_list(user):
    """
    Function that returns a list of permission code names for a given user.
    :param user: user to return the permission list for
    :return:
    """
    # note: m2m cannot be selected
    queryset = Permission.objects.all().select_related('content_type')
    return [
        '{0!s}'.format(p.codename)
        for p in queryset.filter(Q(user=user) | Q(group__user=user))
    ]
