from rest_framework.filters import DjangoObjectPermissionsFilter

from djam.permissions.utils import ownership_based_filter
from djam.permissions.mixins import GenericPermissionsMixin


class OwnerOrModelPermissionsFilter(GenericPermissionsMixin,
                                    DjangoObjectPermissionsFilter):

    def filter_queryset(self, request, queryset, view):
        user = request.user
        method = request.method
        model_cls = queryset.model

        if user.is_superuser:
            return queryset

        required_perms = self.get_strict_permissions(method, model_cls)
        action_perms = user.has_perms(required_perms)
        if action_perms:
            return queryset

        return ownership_based_filter(user, queryset)
