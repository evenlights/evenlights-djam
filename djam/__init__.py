# -*- coding: utf-8 -*-

"""Top-level package for Djam Library."""

__author__ = """George Makarovsky"""
__email__ = 'dev@evenlights.com'
__version__ = '1.1.0'
