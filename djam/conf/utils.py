import os
import json

BOOL_TRUE_VALUES = ['true', '1', 'yes']
BOOL_FALSE_VALUES = ['false', '0', 'no']


class EnvSettings:
    __notification_emitted = False
    __required_keys = []
    __missing_keys = []

    class RequiredKeyMissing(Exception):
        pass

    @classmethod
    def _get_setting(cls, settings, key):

        # dict setting
        if '.' in key:
            tokens = key.split('.')
            pointer = getattr(settings, tokens[0], None)

            if pointer is None:
                return

            for getter in tokens[1:]:
                pointer = pointer.get(getter)

                if pointer is None:
                    return

            return pointer

        return getattr(settings, key, None)

    @classmethod
    def _get_env(cls, key, default=None, target=None, required=False,
                 process=None, exceptions=()):
        target = target or key

        if required:
            cls.__required_keys.append(target)

        value = os.getenv(key, None)

        if value is not None:

            if process:
                try:
                    return process(value)
                except exceptions:
                    raise

            return value

        return default

    # Getters

    @classmethod
    def get(cls, key, **kwargs):
        return cls._get_env(key, **kwargs)

    @classmethod
    def get_int(cls, key, **kwargs):
        return cls._get_env(
            key, process=int, exceptions=(ValueError, TypeError), **kwargs
        )

    @classmethod
    def get_json(cls, key, **kwargs):
        return cls._get_env(
            key, process=json.loads, exceptions=(json.JSONDecodeError,),
            **kwargs
        )

    @classmethod
    def get_bool(cls, key, **kwargs):

        def parse_bool(value):
            if value.lower() in BOOL_TRUE_VALUES:
                return True
            elif value.lower() in BOOL_FALSE_VALUES:
                return False

        return cls._get_env(key, process=parse_bool, **kwargs)

    @classmethod
    def get_list(cls, key, **kwargs):

        def parse_list(value):
            return list(map(lambda i: i.strip(), value.split(',')))

        return cls._get_env(key, process=parse_list, **kwargs)

    # Integrity

    @classmethod
    def check_required(cls):
        from django.conf import settings

        for key in cls.__required_keys:

            if not cls._get_setting(settings, key):
                cls.__missing_keys.append(key)

        if cls.__missing_keys:
            raise cls.RequiredKeyMissing(
                'Following required env keys were not provided through the '
                'env nor were they overridden: {}'.format(
                    ', '.join(cls.__missing_keys)
                )
            )

        cls.emit_notification()

    @classmethod
    def emit_notification(cls):
        from django.conf import settings
        if not cls.__notification_emitted:
            print('-'*80)
            print('Env settings fine')
            print('Version: {}'.format(settings.VERSION))
            cls.__notification_emitted = True
