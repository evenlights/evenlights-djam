import json

from django.conf import settings
from rest_framework import generics, permissions, serializers

from djam.core.views import UnifiedResponseMixin


def get_safe(value):
    safe_types = (int, str, bool, float, list, tuple, dict,)

    if not isinstance(value, safe_types):
        return str(value)

    elif isinstance(value, dict):
        new_value = {**value}
        for key in new_value.keys():
            new_value[key] = get_safe(new_value[key])
        return new_value

    elif isinstance(value, (list, tuple, set)):
        return list(map(get_safe, value))

    return json.loads(json.dumps(value))


class SystemSettingsView(UnifiedResponseMixin, generics.GenericAPIView):
    permission_classes = (
        permissions.IsAuthenticated, permissions.IsAdminUser,
    )

    def get(self, request, *args, **kwargs):
        all_settings = {}
        for attr in dir(settings):
            value = getattr(settings, attr)
            all_settings[attr] = value

        return self.info_response(data=get_safe(all_settings))


class SystemVersionView(UnifiedResponseMixin, generics.GenericAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = serializers.Serializer

    def get(self, request, *args, **kwargs):
        response = {
            'version': settings.VERSION
        }
        return self.info_response(data=response)


class EchoView(UnifiedResponseMixin, generics.GenericAPIView):
    authentication_classes = ()
    permission_classes = ()
    serializer_class = serializers.Serializer

    def get(self, request, *args, **kwargs):
        echo = self.mirrior_request(request)
        print(echo)
        return self.info_response(data=echo)

    def post(self, request, *args, **kwargs):
        echo = self.mirrior_request(request)
        print(echo)
        return self.info_response(data=echo)

    def put(self, request, *args, **kwargs):
        echo = self.mirrior_request(request)
        print(echo)
        return self.info_response(data=echo)

    def patch(self, request, *args, **kwargs):
        echo = self.mirrior_request(request)
        print(echo)
        return self.info_response(data=echo)

    def mirrior_request(self, request):
        response = {
            'GET': request.GET.copy(),
            'POST': request.POST.copy(),
            'META': request.META.copy(),
            'data': request.data.copy(),
        }
        return get_safe(response)
