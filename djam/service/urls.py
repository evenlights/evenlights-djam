from django.urls import path

from djam.service.views import SystemSettingsView, SystemVersionView, EchoView


urlpatterns = [
    path('settings/', SystemSettingsView.as_view(), name='settings'),
    path('version/', SystemVersionView.as_view(), name='version'),
    path('echo/', EchoView.as_view(), name='echo'),
]
