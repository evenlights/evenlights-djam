from django.apps import AppConfig


class ServiceConfig(AppConfig):
    name = 'djam.service'
    label = 'djanm_service'
