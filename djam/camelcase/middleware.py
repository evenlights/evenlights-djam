import dateutil.parser as parser

from django.http import QueryDict
from djangorestframework_camel_case import util


class CamelCaseQueryParamsMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.GET._mutable = True
        request.GET = self.underscoreize(request.GET)
        request.GET._mutable = False

        return self.get_response(request)

    def underscoreize(self, data):

        if isinstance(data, QueryDict):
            new_dict = QueryDict()
            new_dict._mutable = True
            for key, value in data.items():
                new_key = util.camel_to_underscore(key)
                new_dict[new_key] = self.underscoreize(value)

            new_dict._mutable = False
            return new_dict

        if isinstance(data, dict):
            new_dict = {}
            for key, value in data.items():
                new_key = util.camel_to_underscore(key)
                new_dict[new_key] = self.underscoreize(value)
            return new_dict

        if isinstance(data, (list, tuple)):
            for i in range(len(data)):
                data[i] = self.underscoreize(data[i])
            return data

        if isinstance(data, str):
            is_date = False
            try:
                parser.parse(data)
                is_date = True
            except ValueError:
                pass
            return util.camel_to_underscore(data) if not is_date else data

        return data
