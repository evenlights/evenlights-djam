from djangorestframework_camel_case.render import (
    CamelCaseJSONRenderer as BaseCamelCaseJSONRenderer
)


class CamelCaseJSONRenderer(BaseCamelCaseJSONRenderer):
    format = 'jsonCamelCased'
