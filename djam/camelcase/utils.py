import re

from djangorestframework_camel_case.util import (
    underscoreToCamel, camelize as camelize_dict,
    underscoreize as decamelizae_dict
)


def camelize_string(string):
    return re.sub(r'[a-z]_[a-z]', underscoreToCamel, string)
