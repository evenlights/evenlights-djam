from django.http.request import HttpRequest


def get_request_shadow(request):
    """ Method returns request shadow copy usable for patching. The
    resulting `ShadowRequest` object will only have a data filed set.

    :param request: original request to take data from
    :return: `ShadowRequest` instance with the only data field
    """
    new_request = HttpRequest()
    setattr(new_request, 'user', request.user)
    setattr(new_request, 'data', request.data)
    setattr(new_request, 'encoding', request.encoding)
    setattr(new_request, 'accepted_renderer', request.accepted_renderer)
    setattr(new_request, 'GET', request.GET.copy())
    setattr(new_request, 'POST', request.POST.copy())
    setattr(new_request, 'META', request.META.copy())
    setattr(new_request, 'get_full_path', request.get_full_path)
    setattr(new_request, 'method', request.method)

    return new_request
