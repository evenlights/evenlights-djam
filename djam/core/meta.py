

def set_key(model, key, value):
    meta = model.meta

    if meta is None:
        meta = {}

    meta[key] = value
    model.meta = meta


def update_key(model, key, new_value):
    meta = model.meta
    old_value = meta[key]
    if isinstance(old_value, dict):
        old_value.update(new_value)
    else:
        old_value = new_value

    meta[key] = old_value
    model.meta = meta


def get_key(model, key):
    if model.meta:
        return model.meta.get(key)


def remove_key(model, key):
    meta = model.meta
    del meta[key]
    model.meta = meta


class MetaTargetViewMixin(object):
    meta_target = None

    def meta_target_set(self, instance, value, target=None):
        target = target or self.meta_target
        set_key(instance, target, value)

    def meta_target_get(self, instance, key, target=None):
        target = target or self.meta_target
        if instance.meta:
            try:
                return instance.meta[target][key]
            except KeyError:
                pass

    def meta_target_update(self, instance, value, target=None):
        target = target or self.meta_target
        update_key(instance, target, value)

    def meta_target_delete(self, instance, key, target=None):
        target = target or self.meta_target
        meta = instance.meta
        del meta[target][key]
        instance.meta = meta

    def meta_target_remove(self, instance, target=None):
        target = target or self.meta_target
        remove_key(instance, target)
