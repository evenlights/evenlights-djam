import copy
from collections import OrderedDict

from rest_framework.routers import DefaultRouter, SimpleRouter
from rest_framework_nested.routers import NestedSimpleRouter


class APIRootView(DefaultRouter.APIRootView):
    """
    FieldWatch API Root

    For more details refer to [documentation:](http://localhost:8000/docs/)
    """


class ApiRouter(DefaultRouter):
    """
    Allows to register additional api root links which you add to urlpatterns.
    You can register additional links, for example:

    api_root_links={
        'user': 'user-profile',
    }

    'user' is a prefix of the api
    'user-profile' is a name of the urlpatterns
    """
    # APIRootView = APIRootView
    routes = copy.deepcopy(DefaultRouter.routes)
    routes[0].mapping.update({
        'put': 'bulk_update',
        'patch': 'partial_bulk_update',
    })

    api_root_links = None

    def __init__(self, *args, **kwargs):
        self.api_root_links = kwargs.pop('api_root_links', {})
        super(ApiRouter, self).__init__(*args, **kwargs)

    def get_api_root_view(self, api_urls=None):
        """
        Return a basic root view.
        """
        api_root_dict = OrderedDict()
        list_name = self.routes[0].name

        for prefix, url_name in self.api_root_links.items():
            api_root_dict[prefix] = url_name

        for prefix, viewset, basename in self.registry:
            api_root_dict[prefix] = list_name.format(basename=basename)

        return self.APIRootView.as_view(api_root_dict=api_root_dict)


class BulkNestedSimpleRouter(NestedSimpleRouter):
    """
    Map http methods to actions defined on the bulk mixins.
    """
    routes = copy.deepcopy(SimpleRouter.routes)
    routes[0].mapping.update({
        'put': 'bulk_update',
        'patch': 'partial_bulk_update',
    })
