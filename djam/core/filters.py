from rest_framework.filters import OrderingFilter


class ExtendedOrderingFilter(OrderingFilter):
    order_prefixes = ('-',)
    custom_fields = None

    def __init__(self):
        super().__init__()
        self.custom_fields = self.custom_fields or {}

    def resolve_query_param(self, param):
        order_prefix = param[0] if param[0] in self.order_prefixes else None

        if order_prefix:
            param = param[1:]

        if param in self.custom_fields:
            param = self.custom_fields[param]

        if order_prefix:
            param = order_prefix + param

        return param

    def get_valid_fields(self, queryset, view, context=None):
        context = context or {}
        valid_fields = list(super().get_valid_fields(queryset, view, context))
        for field_index, field_info in enumerate(valid_fields):
            if field_info[0] in self.custom_fields:
                valid_fields[field_index] = (
                    self.resolve_query_param(field_info[0]), field_info[1]
                )
        return valid_fields

    def get_ordering(self, request, queryset, view):
        """
        The method is mostly identical to the original one except the ordering
        field resolution logic that it adds atop of it tu support custom field
        resolution and substitution in the resulting query.
        """
        params = request.query_params.get(self.ordering_param)
        if params:
            fields = [
                self.resolve_query_param(param.strip())
                for param in params.split(',')
            ]
            ordering = self.remove_invalid_fields(queryset, fields, view, request)
            if ordering:
                return ordering

        return self.get_default_ordering(view)
