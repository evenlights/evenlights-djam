from rest_framework import pagination
from rest_framework.response import Response


class DefaultPagination(pagination.PageNumberPagination):
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'results': data
        })

    def paginate_queryset(self, queryset, request, view=None):
        page_size_raw = request.query_params.get(
            self.page_size_query_param, None
        )
        if page_size_raw == 'all':
            page_size = queryset.count() or self.page_size
            paginator = self.django_paginator_class(queryset, page_size)
            page_number = 1
            self.page = paginator.page(page_number)
            self.request = request
            return list(self.page)

        return super().paginate_queryset(queryset, request, view=view)
