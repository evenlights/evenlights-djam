import string
from abc import ABCMeta

import random


class BaseStringGenerator(object):
    __metaclass__ = ABCMeta

    def __init__(self, charset=None, length=16):
        super().__init__()

        self.charset = charset or (
            string.ascii_uppercase +
            string.ascii_lowercase +
            string.digits
        )
        self.length = length

    def __call__(self, *args, **kwargs):
        return self.generate()

    def generate(self):
        return ''.join(random.choices(self.charset, k=self.length))


class InviteCodeGenerator(BaseStringGenerator):
    pass


class SMSVerificationCodeGenerator(BaseStringGenerator):

    def __init__(self, charset=None, length=None):
        super().__init__()

        self.charset = charset or string.digits
        self.length = length or 6


def generate_invite_code():
    return InviteCodeGenerator().generate()


def generate_secret_key():
    return BaseStringGenerator(length=32).generate()


def generate_random_string():
    return BaseStringGenerator().generate()


def generte_verification_code():
    return SMSVerificationCodeGenerator().generate()
