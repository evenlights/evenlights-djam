import coreapi
from django.db import models
from rest_framework import (
    generics, status as http_status, serializers, mixins, viewsets,
)
from rest_framework.response import Response
from rest_framework.schemas import AutoSchema

from djam.core.errors import ImproperlyConfigured
from djam.core.serializers import (
    get_negative_random_id, BulkActionSerializer
)


class NestedViewSetMixin:
    """
    Allows creating ViewSet for child objects depending on a parent object.
    For example /organizations/1/settings/ - settings object depends on the
    organization.

    `parent_models`: will be used for deriving parent PKs and IDs from url
      params and building serializer/model field keys.
    """
    parent_models = None
    raise_on_no_parent_keys = False

    def __init__(self, *args, **kwargs):
        if not self.parent_models:
            self.parent_models = []

        # capture parent models
        self._parent_dicts = [
            self.get_parent_dict(m) for m in self.parent_models
        ]

        super().__init__(*args, **kwargs)

    def get_parent_dict(self, entry):
        params = {}
        if isinstance(entry, models.base.ModelBase):
            params.update({
                'model': entry,
                'model_name': entry._meta.model_name,
                'lookup_field': 'pk',
                'lookup_key': '{0}_pk'.format(entry._meta.model_name),
            })
        elif isinstance(entry, dict):
            if 'model' not in entry:
                raise ImproperlyConfigured(
                    'required `model` entry is missing '
                    'in the parent declaration: {0}'.format(entry)
                )

            # defaults
            params.update({
                'model': entry['model'],
                'lookup_field': 'pk',
                'model_name':
                    entry.get('model_name', entry['model']._meta.model_name),
                'lookup_key': '{0}_pk'.format(
                    entry.get('model_name', entry['model']._meta.model_name)
                ),
            })

            # overrrides
            if 'lookup_field' in entry:
                params.update({'lookup_field': entry['lookup_field']})

            # final
            params.update({
                'lookup_key': '{0}_{1}'.format(
                    params['model_name'], params['lookup_field']
                ),
            })

        else:
            raise ImproperlyConfigured(
                'only Django Model and dict instances allowed '
                'for parent entry, got: {0}'.format(type(entry))
            )

        return params

    def resolve_parent(self, parent_dict):
        lookup_value = self.kwargs.get(parent_dict['lookup_key'], None)

        if not lookup_value:
            if self.raise_on_no_parent_keys:
                raise ValueError(
                    'parent key `{}` is missing in the url params'.format(
                        parent_dict['lookup_key']
                    )
                )
        else:
            # here we rely on the Django's ORM which should resolve FK
            # dependency be it specified on child or parent
            fetch_kwargs = {}
            fetch_kwargs[parent_dict['lookup_field']] = lookup_value

            return generics.get_object_or_404(
                parent_dict['model'], **fetch_kwargs
            )

    def get_queryset(self):
        """ Provides inline child filtering by parent objects. A child might
        have multiple parents.
        """
        queryset = super().get_queryset()
        filter_kwargs = {}

        for parent_dict in self._parent_dicts:
            parent = self.resolve_parent(parent_dict)

            if parent:
                # we only append filter options if a parent is found
                filter_kwargs[parent_dict['model_name']] = parent.id

        if filter_kwargs:
            return queryset.filter(**filter_kwargs)

        return queryset

    def get_serializer(self, *args, **kwargs):
        """ The method provides parent keys on modification for an inline child
        object from the url params passed.
        This allows saving inlined views without passing parent foreign keys
        explicitly adopting them from the url params instead.
        """
        if 'data' in kwargs:

            for parent_dict in self._parent_dicts:
                parent = self.resolve_parent(parent_dict)
                # serializer requires parent keys in the form "incident": 1

                if parent:
                    # we only replace data keys if a parent is found
                    kwargs['data'][parent_dict['model_name']] = parent.id

        return super().get_serializer(*args, **kwargs)


class BulkViewSetMixin:
    """
    Mixin for bulk create and update operations.

    You need used Bulk routers for providing routing to bulk operations,
    for example BulkNestedSimpleRouter.
    """

    def create(self, request, *args, **kwargs):
        bulk = isinstance(request.data, list)

        if not bulk:
            return super(BulkViewSetMixin, self).create(
                request, *args, **kwargs
            )

        else:
            serializer_data = []

            for data in request.data:
                serializer = self.get_serializer(data=data)
                serializer.is_valid(raise_exception=True)
                self.perform_create(serializer)
                serializer_data.append(serializer.data)

            headers = self.get_success_headers(serializer_data)

            return Response(
                serializer_data,
                status=http_status.HTTP_201_CREATED,
                headers=headers
            )

    def bulk_update(self, request, *args, **kwargs):
        serializer_data = []
        partial = kwargs.pop('partial', False)
        instance_mapping = {
            str(i.id): i for i in self.filter_queryset(self.get_queryset())
        }

        # map passed data
        data_mapping = {}
        for i in request.data:
            try:

                # clean id field if empty
                if not str(i['id']):
                    del i['id']

                data_mapping[str(i['id'])] = i
            except KeyError:
                rand_id = get_negative_random_id(data_mapping)
                data_mapping[rand_id] = i

        # update or create
        for object_id, data in data_mapping.items():
            instance = instance_mapping.get(object_id, None)

            if instance is None:
                serializer = self.get_serializer(data=data)
            else:
                serializer = self.get_serializer(
                    instance, data=data, partial=True
                )

            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)
            serializer_data.append(serializer.data)

        # delete those not passed if PUT request
        if not partial:
            for object_id, instance in instance_mapping.items():
                if object_id not in data_mapping:
                    instance.delete()

        return Response(serializer_data, status=http_status.HTTP_200_OK)

    def partial_bulk_update(self, request, *args, **kwargs):
        kwargs['partial'] = True

        return self.bulk_update(request, *args, **kwargs)


class SerializerContextMixin:

    def get_serializer_context(self):
        context = super().get_serializer_context()

        if self.request.user:
            context['user'] = self.request.user

        return context


class UnifiedResponseMixin:

    def info_response(self, detail=None, data=None, status=None, headers=None):
        response_data = None

        if data is not None:
            response_data = data

        if detail is not None:
            if not response_data:
                response_data = {'detail': detail}
            else:
                response_data.update({'detail': detail})

        if (status is None) and (response_data is None):
            status = http_status.HTTP_204_NO_CONTENT
        elif status is None and response_data:
            status = http_status.HTTP_200_OK

        return Response(data=response_data, status=status, headers=headers)

    def error_response(self, detail=None, data=None,
                       status=http_status.HTTP_400_BAD_REQUEST):
        response_data = None

        if data is not None:
            response_data = data

        if detail is not None:
            if not response_data:
                response_data = {'detail': detail}
            else:
                response_data.update({'detail': detail})

        return Response(data=response_data, status=status)

    def get_create_response(self, serializer, **kwargs):
        return Response(serializer.data, **kwargs)

    def get_update_response(self, serializer, **kwargs):
        return Response(serializer.data, **kwargs)


class ActionViewMixin(SerializerContextMixin, UnifiedResponseMixin):
    serializer_class = serializers.Serializer

    def _action(self, serializer):
        raise NotImplementedError()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return self._action(serializer)


class BulkActionViewMixin(ActionViewMixin):
    serializer_class = BulkActionSerializer


class FeedViewMixin(UnifiedResponseMixin):
    schema = AutoSchema(
        manual_fields=[
            coreapi.Field(
                'start_from',
                required=True,
                location='query',
                description='Starting element ID of the feed.'
            ),
            coreapi.Field(
                'count',
                required=True,
                location='query',
                description='Count of elements to load.'
            ),
        ]
    )

    def get(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        obj_index = list(map(lambda i: str(i.id), queryset))

        # start_from param
        start_from = request.query_params.get('start_from', None)
        if not start_from:
            return self.error_response(
                detail='`start_from` parameter is required'
            )

        if start_from not in obj_index:
            return self.error_response(
                status=http_status.HTTP_404_NOT_FOUND,
                detail='starting id not found: {}'.format(start_from)
            )

        # count param
        count = request.query_params.get('count', None)
        if not count:
            return self.error_response(
                detail='`count` parameter is required'
            )
        try:
            count = int(count)
        except ValueError:
            return self.error_response(
                detail='`count` should be an integer value'
            )

        start_index = obj_index.index(start_from) + 1
        elements_remaining = len(obj_index) - start_index
        count = count if count <= elements_remaining else elements_remaining
        target_list = obj_index[start_index:start_index+count]

        queryset = queryset.filter(id__in=target_list)
        serializer = self.get_serializer(queryset, many=True)

        return self.info_response(data=serializer.data)


class ListRetrieveUpdateDestroyViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                  mixins.ListModelMixin, viewsets.GenericViewSet):
    pass


class CreateWithRelatedMixin(UnifiedResponseMixin, mixins.CreateModelMixin):
    """
    The :class:`CreateWithRelatedMixin` allows creating models with M2M or
    ForeignKey relation fields, which must be carried out after the root object
    creation. For that purpose, the respective fields are popped from the
    data and processed afterwards.

    Names of the fields to be processed are specified in the ``related_fields``
    property of the view class.
    """
    def create(self, request, *args, **kwargs):
        data = dict(request.data)
        related_fields = getattr(self, 'related_fields', [])
        related_data = {}

        for field_name in related_fields:
            try:
                related_data[field_name] = data.pop(field_name)
            except KeyError:
                pass

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        instance = serializer.instance

        if related_data.keys():
            for field_name in related_fields:
                related_manager = getattr(instance, field_name)
                Related = related_manager.model

                related_objects = Related.objects.filter(
                    id__in=related_data[field_name]
                )
                related_manager.add(*related_objects)

        headers = self.get_success_headers(serializer.data)

        return self.get_create_response(
            serializer, status=http_status.HTTP_201_CREATED,
            headers=headers
        )


class UpdateWithRelatedMixin(UnifiedResponseMixin, mixins.UpdateModelMixin):
    """
    The :class:`UpdateWithRelatedMixin` allows updating models with M2M or
    ForeignKey relation fields, which must be carried out after the root object
    update. For that purpose, the respective fields are popped from
    the request data and processed afterwards.

    Names of the fields to be processed are specified in the ``related_fields``
    property of the view class.

    Update rules:

    * full update (``PUT``) adds all missing objects to the M2M and
      removes all that were no passed

    * partial update (``PATCH``) only adds those missing
    """
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        data = dict(request.data)
        related_fields = getattr(self, 'related_fields', [])
        related_data = {}

        for field_name in related_fields:
            try:
                related_data[field_name] = data.pop(field_name)
            except KeyError:
                pass

        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        if related_data.keys():
            for field_name in related_data:
                related_manager = getattr(instance, field_name)
                Related = related_manager.model

                existing_ids = list((str(o.id) for o in related_manager.all()))
                new_ids = list((str(i) for i in related_data[field_name]))

                # add missing
                for related_id in new_ids:
                    if related_id not in existing_ids:
                        related_instance = Related.objects.get(id=related_id)
                        related_manager.add(related_instance)

                # remove redundant
                for existing_id in existing_ids:
                    if existing_id not in new_ids:
                        related_instance = Related.objects.get(id=existing_id)
                        related_manager.remove(related_instance)

        return self.get_update_response(serializer)


class CreateAndUpdateWithRelatedMixin(CreateWithRelatedMixin,
                                      UpdateWithRelatedMixin):
    pass


class EnhancedSerializerControlMixin(UnifiedResponseMixin):

    def get_serializer(self, *args, action=None, **kwargs):

        if action is None:
            if self.request.method in ('POST',):
                action = 'create'
            elif self.request.method in ('GET',):
                action = 'view'
            elif self.request.method in ('PUT', 'PATCH',):
                action = 'update'

        serializer_class = self.get_serializer_class(action=action)
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self, action=None):
        serializer_class_field = '{}_serializer_class'.format(action) \
            if action else 'serializer_class'
        serializer_class = getattr(self, serializer_class_field, None)

        return serializer_class or super().get_serializer_class()

    def get_create_response(self, serializer, **kwargs):
        # get view representation before returning response
        serializer = self.get_serializer(serializer.instance, action='view')
        return super().get_create_response(serializer, **kwargs)

    def get_update_response(self, serializer, **kwargs):
        # get view representation before returning response
        serializer = self.get_serializer(serializer.instance, action='view')
        return super().get_update_response(serializer, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return self.get_create_response(
            serializer,
            status=http_status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return self.get_update_response(serializer)


class ModelViewSetWithHooks(viewsets.ModelViewSet):
    pre_validate_create_hooks = ('pre_validate_create_hook',)
    pre_create_hooks = ('pre_create_hook',)
    post_create_hooks = ('post_create_hook',)
    pre_validate_update_hooks = ('pre_validate_update_hook',)
    pre_update_hooks = ('pre_update_hook',)
    post_update_hooks = ('post_update_hook',)
    pre_destroy_hooks = ('pre_destroy_hook',)

    def create(self, request, *args, **kwargs):

        for hook in self.pre_validate_create_hooks:
            hook_response = getattr(self, hook)(request)
            if hook_response:
                return hook_response

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        for hook in self.pre_create_hooks:
            hook_response = getattr(self, hook)(request, serializer)
            if hook_response:
                return hook_response

        self.perform_create(serializer)

        instance = serializer.instance
        for hook in self.post_create_hooks:
            hook_response = getattr(self, hook)(request, serializer, instance)
            if hook_response:
                return hook_response

        headers = self.get_success_headers(serializer.data)

        return Response(
            serializer.data,
            status=http_status.HTTP_201_CREATED, headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        for hook in self.pre_validate_update_hooks:
            hook_response = getattr(self, hook)(
                request, instance, partial=partial
            )
            if hook_response:
                return hook_response

        serializer = self.get_serializer(instance, data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)

        for hook in self.pre_update_hooks:
            hook_response = getattr(self, hook)(
                request, serializer, instance, partial=partial
            )
            if hook_response:
                return hook_response

        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        for hook in self.post_update_hooks:
            hook_response = getattr(self, hook)(
                request, serializer, instance, partial=partial
            )
            if hook_response:
                return hook_response

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        for hook in self.pre_destroy_hooks:
            hook_response = getattr(self, hook)(request, instance)
            if hook_response:
                return hook_response

        self.perform_destroy(instance)
        return Response(status=http_status.HTTP_204_NO_CONTENT)

    def pre_validate_create_hook(self, request):
        pass

    def pre_create_hook(self, request, serializer):
        pass

    def post_create_hook(self, request, serializer, instance):
        pass

    def pre_validate_update_hook(self, request, instance, partial=False):
        pass

    def pre_update_hook(self, request, serializer, instance, partial=False):
        pass

    def post_update_hook(self, request, serializer, instance, partial=False):
        pass

    def pre_destroy_hook(self, request, instance):
        pass

    def generic_error(self, message, error, instance=None):
        return Response(
            data={
                'detail': message,
                'reason': str(error)
            },
            status=http_status.HTTP_400_BAD_REQUEST
        )

    def creation_error(self, message, error, instance):
        instance.delete()
        return self.generic_error(message, error, instance)


class RuntimeMixin:
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._runtime = {}


class ApplyDefaultsMixin:

    def create(self, request, *args, **kwargs):
        self.apply_create_defaults(request)
        return super().create(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        self.apply_update_defaults(request)
        return super().update(request, *args, **kwargs)

    def apply_defaults(self, request):
        return request

    def apply_create_defaults(self, request):
        return self.apply_defaults(request)

    def apply_update_defaults(self, request):
        return self.apply_defaults(request)
