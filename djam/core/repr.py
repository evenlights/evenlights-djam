import json
from rest_framework.renderers import JSONRenderer


def to_transport(serializer):
    return json.loads(JSONRenderer().render(serializer.data))
