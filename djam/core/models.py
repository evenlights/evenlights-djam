import uuid as uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _

from djam.permissions.models import MetaPermissions


class UuidIdModel(models.Model):
    """
    Abstract Base Model
    """

    class Meta(MetaPermissions):
        abstract = True
        ordering = ('-created_timestamp',)

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    created_timestamp = models.DateTimeField(
        _('Created At'), auto_now_add=True
    )
    updated_timestamp = models.DateTimeField(
        _('Updated At'), auto_now=True
    )
