import phonenumbers

from django.conf import settings
from django.core.validators import RegexValidator
from django.db import models

try:
    from django.contrib.postgres.fields import JSONField
except ImportError:
    from django_mysql.models import JSONField


class PhoneValidator(RegexValidator):
    """ Phone number regex validator comprises the following tokens:

    (\+\d|00\d+)?           either +n or 00n, optional
    [\s-]*                  separator, any number of spaces or "-" signs,
                            optional
    [\(\[]?(\d{3})[\)\]]?   area code,  3 digits, in braces or not
                            e.g. 800, (800) or [800]
    [\s-]*                  separator, any number of spaces or "-" signs,
                            optional
    (\d{3})                 trunk is 3 digits (e.g. '555')
    [\s-]*                  separator, any number of spaces or "-" signs,
                            optional
    (\d{2})                 trunk is 3 digits (e.g. '555')
    [\s-]*                  separator, any number of spaces or "-" signs,
                            optional
    (\d{2})                 rest 2 digits
    [\s-]*                  separator, any number of spaces or "-" signs,
                            optional
    (\d{2})                 rest 2 digits
    [\s-]*                  separator, any number of spaces, optional
    (\d*)                   extension is optional and can be any number of
                            digits
    """
    regex = r'^(\+\d|00\d+)? *\(?\d{3}\)?-? *\d{3}-? *-?\d{2}-? *\d{2} *(\d{1})*'
    message = 'Phone number should format as following: +1 123 123-12-34. ' \
              'Minus and space can be used as a separator signs, ' \
              'brackets on first group allowed'


class PhoneField(models.CharField):

    default_validators = [PhoneValidator()] if not settings.DEBUG else []
    description = 'Phone number'

    def get_prep_value(self, value):

        number_parsed = None
        try:
            number_parsed = phonenumbers.parse(value, region='US')
        except phonenumbers.NumberParseException:
            try:
                number_parsed = phonenumbers.parse(value, region=None)
            except phonenumbers.NumberParseException:
                pass

        if number_parsed:
            value = phonenumbers.format_number(
                number_parsed,
                phonenumbers.PhoneNumberFormat.INTERNATIONAL
            )

        return value


