import random

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.utils import model_meta


def get_negative_random_id(data_map):
    random_id = random.randint(-10000, -1)
    while random_id in data_map:
        random_id = random.randint(-10000, -1)
    return random_id


class BulkActionSerializer(serializers.Serializer):
    objects = serializers.ListField(child=serializers.UUIDField())


class HumanReadableChoiceField(serializers.ChoiceField):

    def __init__(self, choices, **kwargs):
        if choices.__class__.__name__ in ('generator', 'function'):
            self._lazy_choices = choices
            choices = ()
        super().__init__(choices=choices, **kwargs)

    def to_representation(self, obj):
        self._check_lazy_choices()
        try:
            return self._choices[obj]
        except KeyError:
            return self._choices[obj.id]

    def to_internal_value(self, data):
        self._check_lazy_choices()

        if isinstance(data, str):
            data = data.strip()
            return self._find_key(self._choices, data)
        else:
            return data

    def _check_lazy_choices(self):
        try:
            self._set_choices(tuple(self._lazy_choices()))
            delattr(self, '_lazy_choices')
        except AttributeError:
            pass

    def _find_key(self, input_dict, value):
        try:
            return list(k for k, v in input_dict.items() if v == value)[0]
        except IndexError:
            self.fail('invalid_choice', input=value)


class ModelCleanMixin:

    def validate(self, attrs):
        request = self.context.get('request', None)

        if request and request.method in ('POST', 'PUT'):
            instance = self.Meta.model(**attrs)

            try:
                instance.clean()
            except ValidationError as e:
                raise serializers.ValidationError(e.args[0])

        elif request.method == 'PATCH':
            instance = self.instance
            info = model_meta.get_field_info(instance)
            for attr, value in attrs.items():
                if attr in info.relations and info.relations[attr].to_many:
                    field = getattr(instance, attr)
                    field.set(value)
                else:
                    setattr(instance, attr, value)
            try:
                instance.clean()
            except ValidationError as e:
                raise serializers.ValidationError(e.args[0])

        return super().validate(attrs)


class ChildCRUDSerializerMixin:
    """
    Serializer mixin is used to create child objects *along* with the parent. In
    contrast to "create with related" paradigm where related objects already
    exist, this mixin implements a "create with children" pattern.

    In order to enable the serializer behaviour a special meta field needs to
    be set:

    .. code:: python

        class ExampleSerializer(ChildCRUDSerializerMixin, ModelSerializer):
            children = ChildSerializer(many=True, required=False)

            class Meta:
                model = Example
                fields = '__all__'
                children = {
                    'children': {
                        'check_on_validate': False,
                        'assume_from_parent': (('parent', '.',),),
                        'unique_key': ('name',),
                        'required_keys': ('name',),
                    }
                }

    Note the following field configuration parameters:

    :param check_on_validate: and object that children depend on might not exist
      at the moment of creation, thus validation of child data can be disabled
      and  postponed until the actual creation.

    :param assume_from_parent: a list of fields to assume from the parent
      objects in the form of ``('field_on_child', 'name_on_parent',)``. A ``.``
      in the name of parent will mean the parent itself.

    :param unique_key: a list of fields that objects should be identified by

    :param required_keys: a list of required fields

    If ``unique_key`` is one field and is equal to ``required_keys`` you can use
    the shortened representation, i.e. pass a string instead of an dictionary.
    """
    def __init__(self, instance=None, data=serializers.empty, **kwargs):
        child_data = {}
        if data is not serializers.empty:
            for field_name in self.Meta.children.keys():
                if field_name in data:
                    child_data[field_name] = data.pop(field_name)
        super(ChildCRUDSerializerMixin, self).__init__(
            instance=instance, data=data, **kwargs
        )
        self.context.update(child_data)

    def validate(self, attrs):
        for field_name in self.Meta.children.keys():
            if field_name in self.context:
                field_config = self.Meta.children[field_name]
                self.context[field_name] = list([
                    self._get_child_data(field_name, field_config, index, obj)
                    for index, obj in enumerate(self.context[field_name])
                ])
        return super().validate(attrs)

    def create(self, validated_data):
        instance = super().create(validated_data)

        for field_name in self.Meta.children.keys():
            field_config = self.Meta.children[field_name]
            child_data = self.context.pop(field_name, [])

            for index, data in enumerate(child_data):

                for parent_field in field_config.get('assume_from_parent', []):
                    target_name, source_name = parent_field
                    data[target_name] = self._get_parent_value(
                        instance, source_name
                    )

                self.create_child(
                    instance, validated_data,
                    field_name, field_config, index, data
                )

        return instance

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)

        for field_name in self.Meta.children.keys():
            field_config = self.Meta.children[field_name]
            instance_mapping = dict({
                str(i.id): i for i in getattr(instance, field_name).all()
            })

            # map passed data
            data_mapping = {}
            for i in self.context.pop(field_name, []):
                try:
                    # clean id field if empty
                    if not str(i['id']):
                        del i['id']
                    data_mapping[str(i['id'])] = i

                except KeyError:
                    rand_id = get_negative_random_id(data_mapping)
                    data_mapping[rand_id] = i

            # update or create
            for index, item in enumerate(data_mapping.items()):
                child_id, child_data = item
                child_instance = instance_mapping.get(child_id, None)

                if child_instance is None:
                    for parent_field in field_config.get(
                            'assume_from_parent', []
                    ):
                        target_name, source_name = parent_field
                        child_data[target_name] = self._get_parent_value(
                            instance, source_name
                        )

                    self.create_child(
                        instance, validated_data,
                        field_name, field_config, index, child_data
                    )
                else:
                    self.update_child(
                        instance, validated_data,
                        field_name, field_config, index, child_instance,
                        child_data
                    )

            # delete those not passed if PUT request
            if not self.partial:
                for child_id, child_instance in instance_mapping.items():
                    if child_id not in data_mapping:
                        self.delete_child(
                            instance, validated_data,
                            field_name, field_config, child_instance
                        )

        return instance

    def get_child_serializer_class(self, field_name):
        field_serializer = self.get_fields()[field_name].child
        return type(field_serializer)

    def create_child(self, parent_instance, parent_validated_data,
                     field_name, field_config, index, child_validated_data):
        serializer_class = self.get_child_serializer_class(field_name)
        serializer = serializer_class(data=child_validated_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

    def update_child(self, parent_instance, parent_validated_data,
                     field_name, field_config, index, child_instance,
                     child_validated_data):
        serializer_class = self.get_child_serializer_class(field_name)
        serializer = serializer_class(
            child_instance, data=child_validated_data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

    def delete_child(self, parent_instance, parent_validated_data,
                     field_name, field_config, child_instance):
        child_instance.delete()

    def _get_child_data(self, field_name, field_config, index, obj):

        if isinstance(obj, str):
            if len(field_config['unique_key']) == 1 and \
                    field_config['unique_key'] == field_config['required_keys']:
                data = {field_config['unique_key'][0]: obj}
            else:
                raise serializers.ValidationError({
                    '{}[{}]'.format(field_name, index): _(
                        'passing child value as shortened string '
                        'representation is only allowed '
                        'if `unique_key` has length 1 '
                        'and it is the same as `required_keys`'
                    )
                })

        elif isinstance(obj, dict):
            data = obj

        else:
            raise serializers.ValidationError({
                '{}[{}]'.format(field_name, index): _(
                    'unsupported child representation, only strings and dicts '
                    'allowed'
                )
            })

        if field_config.get('check_on_validate', False):
            serializer_class = self.get_child_serializer_class(field_name)
            serializer = serializer_class(data=data)
            try:
                serializer.is_valid(raise_exception=True)
            except serializers.ValidationError:
                raise serializers.ValidationError({
                    '{}[{}]'.format(field_name, index): serializer.errors
                })

        return data

    def _get_parent_value(self, instance, source_name):
        if source_name == '.':
            value = str(instance.id)
        else:
            value = getattr(instance, source_name)
            if hasattr(value, 'id'):
                value = str(value.id)

        return value
