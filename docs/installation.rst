.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Djam, run this command in your terminal:

.. code-block:: console

    $ pip install https://s3.console.aws.amazon.com/s3/buckets/evenlights-release/libs/djam/djam-<version>.tar.gz

Or if you prefer a ``whl`` version:

.. code-block:: console

    $ pip install https://s3.amazonaws.com/evenlights-release/libs/djam/djam-<version>-py2.py3-none-any.whl

This is the preferred method to install Djam, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Djam can be downloaded from the `Bitbucket repo`_.

You can either clone the public repository:

.. code-block:: console

    $ hg clone ssh://hg@bitbucket.org/evenlights/evenlights-djam

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://bitbucket.org/evenlights/evenlights-djam/get/master.tar.gz

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Bitbucket repo: https://bitbucket.org/evenlights/evenlights-djam
.. _tarball: https://bitbucket.org/evenlights/evenlights-aws-dj/get/master.tar.gz
