#!/usr/bin/env python
#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
The setup script.
Created from the `pypackage
<https://github.com/audreyr/cookiecutter-pypackage template>`_ template using
the `cookiecutter project<https://github.com/audreyr/cookiecutter>`_.
"""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    # Common
    'pytz',
    'wheel',
    'lxml',
    'requests',
    'setuptools',
    'phonenumbers',
    'python-dateutil',

    # Databases
    'redis',

    # Django
    'Django>=2.0',
    'django-mysql',
    'django-debug-toolbar',
    'django-crispy-forms',
    'django-filter',
    'django-guardian',
    'django-crispy-forms',
    'django-oauth-toolkit',
    'django-timezone-field',
    'django-cors-headers',
    'djoser',

    # DRF
    'djangorestframework',
    'drf-nested-routers',
    'djangorestframework_camel_case',
    'djangorestframework-jwt',
    'djangorestframework-oauth',
    'django-rest-swagger',

    # Documentation
    'coreapi',
    'pygments',
    'markdown',
]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest']

setup(
    author="George Makarovsky",
    author_email='dev@evenlights.com',
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: Other/Proprietary License',
        'Intended Audience :: Developers',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    description="Djam is a generic library containing some frequently used"
                "blocks for Django apps",
    install_requires=requirements,
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='djam',
    name='djam',
    packages=find_packages(include=['djam']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://bitbucket.org/evenlights/evenlights-djam',
    version='1.1.0',
    zip_safe=False,
)
