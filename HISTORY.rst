=======
History
=======


1.0.0 (2018-06-12)
------------------

Initial release. Packages added:

* conf

* core

* backends

* camelcase

* permissions

* service
